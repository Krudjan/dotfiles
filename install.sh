#!/bin/bash

# This is a simple script to install all the software I like to have on my system
# and some other things.
# It is currently only supporting Fedora.

if [[ ! $(grep defaultyes /etc/dnf/dnf.conf) ]]
then
	echo "defaultyes=True" | sudo tee -a /etc/dnf/dnf.conf
fi


# Enable RPM Fusion repositories
# Free
sudo dnf install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
# Nonfree
sudo dnf install \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm


# Install packages from package list
sudo dnf install $(cat package.list) --skip-broken

# Install flatpaks from flatpak list
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak install flathub $(cat flatpak.list)

# install configuration files:
if [[ "$(pwd)" != "$HOME/dotfiles" ]]
then
    cd $HOME/dotfiles
fi

stow -S zsh
stow -S tmux
stow -S nvim
stow -S kitty
stow -S scripts
stow -S zathura

cd ~/

# Install dein.vim
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > ~/Downloads/installer.sh
sh ~/Downloads/installer.sh ~/.config/nvim/dein

# Installing plugins for playing movies and music
sudo dnf install \
    gstreamer1-plugins-{bad-\*,good-\*,base} \
    gstreamer1-plugin-openh264 gstreamer1-libav \
    --exclude=gstreamer1-plugins-bad-free-devel

sudo dnf install lame\* --exclude=lame-devel

sudo dnf group upgrade --with-optional Multimedia

# Install tmux plugin manager (tpm)
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# Shortcuts:
for i in {1..6}
do
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-$i "['<Super>$i']"
    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-$i "['<Super><Shift>$i']"
    gsettings set org.gnome.shell.keybindings switch-to-application-$i "[]"
done

## tiling:
gsettings set org.gnome.desktop.wm.keybindings maximize "['<Super>l']"
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Super>a']"
gsettings set org.gnome.mutter.keybindings toggle-tiled-right "['<Super>e']"
gsettings set org.gnome.mutter.keybindings toggle-tiled-left "['<Super>i']"

## misc:
gsettings set org.gnome.desktop.wm.keybindings close "['<Super><Shift>x', '<Alt>F4']"
add_custom_shortcut () {
    existing_shortcut_string=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
    exst_str_len=${existing_shortcut_string:(-4):1}
    exst_len=$((exst_str_len))
    new_shortcut_index=$((exst_len+1))
    # Assign bindings
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom$new_shortcut_index/ name "$1"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom$new_shortcut_index/ command "$2"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom$new_shortcut_index/ binding "$3"
    # Declare the new binding
    declaration_string="${existing_shortcut_string::-1}, '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom$new_shortcut_index/']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "${declaration_string}"
}
add_custom_shortcut "Terminal" "kitty -e ~/.scripts/tmux.sh" "<Super>Return"

# make zsh the default shell
sudo chsh -s $(which zsh) gregor

