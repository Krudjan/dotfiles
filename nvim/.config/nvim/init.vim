" dein vim Plugins
if &compatible
    set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.config/nvim/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.config/nvim/dein')
    call dein#begin('~/.config/nvim/dein')

    call dein#add('~/.config/nvim/dein/repos/github.com/Shougo/dein.vim')
    " call dein#add('vim-airline/vim-airline')
    call dein#add('itchyny/lightline.vim')
    call dein#add('morhetz/gruvbox')
    call dein#add('fatih/vim-go', { 'build': ':GoUpdateBinaries'})
    call dein#add('lervag/vimtex')
    call dein#add('jiangmiao/auto-pairs')
    call dein#add('neoclide/coc.nvim', {'merged': 0, 'rev': 'release'})
    call dein#add('sirver/UltiSnips')
    call dein#add('honza/vim-snippets')
    call dein#add('junegunn/goyo.vim')
    call dein#add('plasticboy/vim-markdown')
    call dein#add('reedes/vim-pencil')
    call dein#add('tpope/vim-commentary')
    call dein#add('JuliaEditorSupport/julia-vim')
    call dein#add('junegunn/fzf.vim')
    call dein#add('easymotion/vim-easymotion')
    call dein#add('numirias/semshi')
    call dein#add('wsdjeg/dein-ui.vim')

    call dein#end()
    call dein#save_state()
endif

" Coc Lightline integration
function! CocCurrentFunction()
    return get(b:, 'coc_current_function', '')
endfunction

"Coc Highlight
autocmd CursorHold * silent call CocActionAsync('highlight')

let g:lightline = {
      \ 'colorscheme': 'jellybeans',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'currentfunction', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'cocstatus': 'coc#status',
      \   'currentfunction': 'CocCurrentFunction'
      \ },
      \ }

" Some basics:

set autoindent
filetype plugin indent on
syntax on
set path+=**
" colorscheme wal
set encoding=utf-8
set number
set relativenumber
set expandtab
set tabstop=4
set shiftwidth=4
set ttimeoutlen=10
" UI
set showmatch
set hlsearch
set cursorline
set background=dark
set clipboard+=unnamedplus
let g:gruvbox_contrast_light='hard'
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
set colorcolumn=80


" Tmux
imap "[1~" [H
imap "[4~" [F
imap "[1~" <Home>
imap "[4~" <End>
inoremap '\e[1~' <Home>
inoremap '\e[4~' <End>

" Set julia filetype
autocmd BufRead,BufNewFile *.jl :set filetype=julia

" FOlds
set foldenable

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow
set splitright

" Shortcutting split navigation, saving a keypress:
nnoremap <C-i> <C-w>h
nnoremap <C-a> <C-w>j
nnoremap <C-l> <C-w>k
nnoremap <C-e> <C-w>l


" Replace all is aliased to S.
nnoremap S :%s//g<Left><Left>

"For saving view folds:
"au BufWinLeave * mkview
"au BufWinEnter * silent loadview

" Interpret .md files, etc. as .markdown

" Spell-check set to F6:
map <F8> :setlocal spell! spelllang=de,en_gb<CR>
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u


" Goyo plugin makes text more readable when writing prose:
map <F10> :Goyo<CR>
inoremap <F10> <esc>:Goyo<CR>a

" Enable autocompletion:
set wildmode=longest,list,full
set wildmenu

" Automatically deletes all tralling whitespace on save.
autocmd BufWritePre * %s/\s\+$//e

"
nnoremap ge :CocCommand explorer<CR>

" Mapping to exit Terminal mode:

tnoremap <C-e><C-t> <C-\><C-n>

vnoremap K xkP`[V`]
vnoremap J xp`[V`]

"let g:powerline_pycmd="py3"
set laststatus=2
set autoread
set noswapfile

" vim-go
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types =1
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'

"vimtex
let g:tex_flavor='latex'
let g:vimtex_quickfix_mode=0
set conceallevel=2
let g:tex_conceal='abdgms'

" snippets with ultisnips
set runtimepath+=~/.config/nvim/
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" Pipe Selected text to python
vnoremap P :! python3<CR>

" expanded runtimepath
set runtimepath+=**

let g:PowerLine_symbols = 'fancy'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='gruvbox'
let g:indent_guides_guide_size=0
let g:airline#extensions#ale#enabled = 1

let g:coc_global_extensions = [ 'coc-ultisnips', 'coc-jedi', 'coc-rls', 'coc-gocode', 'coc-vimtex', 'coc-json', 'coc-explorer', 'coc-julia', 'coc-highlight']

let g:vim_markdown_folding_disabled = 0
let g:vim_markdown_math = 1
let g:vim_markdown_conceal_code_blocks = 0
autocmd FileType rmd set conceallevel=0


" Rmarkdown
autocmd FileType rmd inoremap <F9> <Esc>:!Rscript -e "rmarkdown::render('%')"<CR>
autocmd FileType rmd nnoremap <F9> :!Rscript -e "rmarkdown::render('%')"<CR>


nmap <C-f> :call CocActionAsync('jumpDefinition')<CR>
nnoremap <C-h> :call CocActionAsync('doHover')<CR>
