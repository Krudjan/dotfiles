#!/usr/bin/env python3

import subprocess

tmux_sessions_output = subprocess.run(['tmux',  'list-sessions'], capture_output=True)

def sanitise(session: str) -> str:
    name = session.split(' ')[0]
    if name is not None:
        return name.rstrip(':')
    else: 
        return session

tmux_sessions = map(sanitise, tmux_sessions_output.stdout.decode('ascii').split('\n'))

if not tmux_sessions:
    subprocess.run(['tmux', 'new'])
else:
    session_output = '\n'.join(tmux_sessions) + '[new]'

    inp = subprocess.Popen(['echo', session_output], stdout=subprocess.PIPE)
    fzf_output = subprocess.run(['fzf', '--preview', '"if [{} != \[new\] ]; then tmux list-window -t {}; fi'], stdin=inp.stdout, shell=True, capture_output=True)
    selected_session = fzf_output.stdout.decode('ascii').strip('\n')
    subprocess.run(['tmux', 'attach', '-t', selected_session])