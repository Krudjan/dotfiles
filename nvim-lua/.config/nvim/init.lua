--[[ 
    Plugins
    -------
]]

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- The actual lazy config 

require('lazy').setup({
  { "ellisonleao/gruvbox.nvim", priority = 1000 },
  { "nvim-lualine/lualine.nvim" },
  { "nvim-tree/nvim-web-devicons" },
  { "lervag/vimtex" },
  { "numToStr/Comment.nvim" },
  { "tpope/vim-surround" },
  { "m4xshen/autoclose.nvim" },
  {
    "L3MON4D3/LuaSnip",
    -- follow latest release.
    version = "1.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
    -- install jsregexp (optional!).
    build = "make install_jsregexp",
    dependencies = { "rafamadriz/friendly-snippets" }
  },
  { "rafamadriz/friendly-snippets" },
  { "hrsh7th/nvim-cmp" },
  { "hrsh7th/cmp-buffer" },
  { "hrsh7th/cmp-path" },
  { "hrsh7th/cmp-nvim-lsp" },
  { "saadparwaiz1/cmp_luasnip" },
  { "neovim/nvim-lspconfig" },
  { "nvim-treesitter/nvim-treesitter" },
  { 
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = { "nvim-lua/plenary.nvim" }
  }
})

require("autoclose").setup()
require("Comment").setup({
  ignore = "^$"
})


-- Apparently you should do this
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
local ls = require("luasnip")
local s = ls.snippet
-- LuaSnip settings
ls.config.set_config({
  history = true, -- keep around last snippet local to jump back
  enable_autosnippets = true,
})
-- require("luasnip.loaders.from_vscode").lazy_load()
require("luasnip.loaders.from_lua").lazy_load({paths = "~/.config/nvim/snippets"})


--[[
    General Settings 
    ----------------
]]

-- Enable relative numbering
vim.opt.number = true
vim.opt.relativenumber = true

-- Automatic indentation 
vim.opt.autoindent = true

-- Encoding (unnecessary?)
vim.opt.encoding = 'utf-8'

-- Automatically expand tabs to 4 spaces
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

-- Default splits on right and bottom
vim.opt.splitbelow = true
vim.opt.splitright = true

-- Use Mouse in every mode
vim.opt.mouse = 'a'

-- Don't highlight all search results
vim.opt.hlsearch = false

-- Highlight matching brackets
vim.opt.showmatch = true

-- Enable folds
vim.opt.foldenable = true

-- Use space as leader key
vim.g.mapleader = ' '
vim.g.maplocalleader = ','

-- Use system clipboard
vim.o.clipboard = "unnamedplus"

-- No swapfile
vim.opt.swapfile = false

-- Use gruvbox theme
vim.cmd.colorscheme('gruvbox')

vim.g.vimtex_syntax_enable = 0
vim.g.vimtex_syntax_conceal_disable = 1


--[[
    Keybindings
    -----------
]]

-- Use Ctrl + iale to navigate splits
vim.keymap.set('n', '<C-i>', '<C-w>h')
vim.keymap.set('n', '<C-a>', '<C-w>j')
vim.keymap.set('n', '<C-l>', '<C-w>k')
vim.keymap.set('n', '<C-e>', '<C-w>l')

-- Use 'S' as a shortcut for search and replace
vim.keymap.set('n', 'S', ':%s//g<Left><Left>')

-- Use 'K' and 'J' to move an entire line up or down when in visual mode
vim.keymap.set('x', 'K', 'xkP`[V`]')
vim.keymap.set('x', 'J', 'xjP`[V`]')

-- Keymaps for telescope
--
vim.keymap.set("n", "<leader>ff", require("telescope.builtin").find_files)
vim.keymap.set("n", "<leader>fb", require("telescope.builtin").buffers)
vim.keymap.set("n", "<leader>fg", require("telescope.builtin").live_grep)


-- Keybinding for switching between dark and light mode: 
vim.g.background = "dark"
local switch_theme = function() 
  if vim.g.background == "dark" then 
    vim.opt.background = "light"
    vim.g.background = "light"
  else
    vim.opt.background = "dark" 
    vim.g.background = "dark" 
  end
end
vim.keymap.set("n", "<leader>st", switch_theme)


--[[
    Lua Line
    --------
]]
require('lualine').setup {
  options = {
    icons_enabled = false,
    theme = 'jellybeans',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {
      statusline = {},
      winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = false,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype', "require'lsp-status'.status()"},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {}
}


--[[
    NVIM CMP
    --------
--]]

local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local cmp = require("cmp")

cmp.setup({
  snippet = {
    expand = function(args)
      ls.lsp_expand(args.body)
    end
  },
  mapping = {
    ["<CR>"] = cmp.mapping.confirm({ select = true}),
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
        -- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable() 
        -- they way you will only jump inside the snippet region
      elseif ls.expand_or_jumpable() then
        ls.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),

    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif ls.jumpable(-1) then
        ls.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),

  },
  sources = cmp.config.sources({
    { name = "nvim_lsp"},
    { name = "luasnip"},
    { name = "buffer" },
    { name = "path" },
  }),
  completion = {
    keyword_length = 3
  },
})


--[[
    LSP Config
    ----------
--]]

local lspconfig = require("lspconfig")
local lsp_defaults = lspconfig.util.default_config
lsp_defaults.capabilities = vim.tbl_deep_extend(
  'force',
  lsp_defaults.capabilities,
  require('cmp_nvim_lsp').default_capabilities()
)

lspconfig.lua_ls.setup({})
lspconfig.pyright.setup({})
lspconfig.texlab.setup{}

vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'LSP actions',
  callback = function()
    local bufmap = function(mode, lhs, rhs)
      local opts = {buffer = true}
      vim.keymap.set(mode, lhs, rhs, opts)
    end

    -- Displays hover information about the symbol under the cursor
    bufmap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>')

    -- Jump to the definition
    bufmap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>')

    -- Jump to declaration
    bufmap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>')

    -- Lists all the implementations for the symbol under the cursor
    bufmap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>')

    -- Jumps to the definition of the type symbol
    bufmap('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>')

    -- Lists all the references 
    bufmap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>')

    -- Displays a function's signature information
    bufmap('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>')

    -- Renames all references to the symbol under the cursor
    bufmap('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>')

    -- Selects a code action available at the current cursor position
    bufmap('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>')
    bufmap('x', '<F4>', '<cmd>lua vim.lsp.buf.range_code_action()<cr>')

    -- Show diagnostics in a floating window
    bufmap('n', 'gl', '<cmd>lua vim.diagnostic.open_float()<cr>')

    -- Move to the previous diagnostic
    bufmap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>')

    -- Move to the next diagnostic
    bufmap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>')
  end
})


--[[
    Tree-Sitter
    -----------
--]]

require("nvim-treesitter.configs").setup {
  ensure_installed = {"c", "lua", "vim", "python", "latex"},
  sync_install = false, 
  highlight = {
    enable = true,
  },
  indent = {
    enable = true,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn", -- set to `false` to disable one of the mappings
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  }
}

--[[
    Vimtex 
    ------
--]]
vim.cmd[[filetype plugin indent on]]
