local ts = require("vim.treesitter")
local ls = require("luasnip")
local MATH_NODES = {
  displayed_equation = true,
  inline_formula = true,
  math_environment = true,
}

local function get_node_at_cursor()
  local buf = vim.api.nvim_get_current_buf()
  local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  row = row - 1
  col = col - 1

  local parser = ts.get_parser(buf, "latex")
  if not parser then
    return
  end
  local root_tree = parser:parse()[1]
  local root = root_tree and root_tree:root()

  if not root then
    return
  end

  return root:named_descendant_for_range(row, col, row, col)
end

function in_text(check_parent)
  local node = get_node_at_cursor()
  while node do
    if node:type() == "text_mode" then
      if check_parent then
        -- For \text{}
        local parent = node:parent()
        if parent and MATH_NODES[parent:type()] then
          return false
        end
      end

      return true
    elseif MATH_NODES[node:type()] then
      return false
    end
    node = node:parent()
  end
  return true
end

function math()
  local node = get_node_at_cursor()
  while node do
    if node:type() == "text_mode" then
      return false
    elseif MATH_NODES[node:type()] then
      return true
    end
    node = node:parent()
  end
  return false
end

local function not_math()
  return not math()
end

local as = ls.extend_decorator.apply(s, { snippetType = "autosnippet" })
local ps = require("luasnip.extras.postfix").postfix
local line_begin = require("luasnip.extras.conditions.expand").line_begin

local M = {
  as({trig="ali", dscr="align* environment"},
    fmta(
      [[
      \begin{align*}
          <>
      \end{align*}
    ]],
      { i(0) }
    ),
    { condition = not_math * line_begin }
  ),
  s({trig="beg", dscr="Insert environment", snippetType="autosnippet"},
    fmta(
      [[
        \begin{<>}
            <>
        \end{<>}
        ]],
      {i(1), i(0), rep(1)}
    ),
    { condition = line_begin }
  ),
  s({trig="mytemplate", dscr="The default template"},
    fmta(
      [[
        \documentclass[a4paper, 10pt]{<>}

        \input{~/Templates/preamble.tex}

        \begin{document}
            <>
        \end{document}
        ]],
      {i(1, "scrartcl"), i(0)}
    ),
    { condition = line_begin }
  ),
  s({trig="fig", dscr="Figure environment"},
    fmta(
      [[
        \begin{figure}[<>]
        \centering
        \includegraphics[width=.8\textwidth]{<>}
        \caption{<>}
        \label{fig:<>}
        \end{figure}
        ]],
      {i(1), i(2), i(3), i(4)}
    )
  ),
  s({trig="enum", dscr="Enum environment"},
    fmta(
      [[
        \begin{enumerate}[label=<>]
            \item <>
        \end{enumerate}
        ]],
      {i(1, [[(\alph*)]]), i(0)}
    ),
    { condition = line_begin}
  ),
  s({trig="item", dscr="Itemize environment"},
    fmta(
      [[
        \begin{itemize}
            \item <>
        \end{itemize}
        ]],
      {i(0)}
    ),
    { condition = line_begin}
  ),
  s({trig="dm", dscr="enter inline math environment", snippetType="autosnippet"},
    fmta(
      [[\(<>\)]],
      {i(0)}
    ),
    { condition = not_math }
  ),
  as({trig="lr()", name="left-right ()"},
    fmta(
      [[\left( <> \right)]],
      {i(0)}
    ),
    { condition = math}
  ),
  as({trig="lr{}", name="left-right {}"},
    fmta(
      [[\left\{ <> \right\}]],
      {i(0)}
    ),
    { condition = math}
  ),
  -- as({trig="lrb", name = "left-right []"},
  --     {t("\\left \["), i(0), t("\\right\]")},
  --     { condition = math}
  -- ),
  as({trig="lra", name="left-right <>"},
    fmta(
      [[\left\langle <> \right\rangle]],
      {i(0)}
    ),
    { condition = math}
  ),
  as({trig="sum", name="sum", dscr="fancy sum"}, 
    fmta(
      [[\sum_{<>}^{<>} <>]],
      {i(1, "i=0"), i(2, "\\infty"), i(0)}
    ),
    { condition = math }
  ),
  as({trig="lim", name="limit", dscr="fancy limit"}, 
    fmta(
      [[\lim_{<> \to <>} <>]],
      {i(1, "n"), i(2, "\\infty"), i(0)}
    ),
    { condition = math }
  ),
  as({trig="prod", name="prod", dscr="fancy prod"}, 
    fmta(
      [[\prod_{<>}^{<>} <>]],
      {i(1, "i=1"), i(2, "\\infty"), i(0)}
    ),
    { condition = math }
  ),
  as({trig="norm", name="Norm"},
    fmta(
      [[\|<>\|<>]],
      {i(1), i(0)}
    ),
    { condition = math }
  ),
  as({trig="tp", name="to the power", wordTrig = false},
    fmta(
      [[^{<>}<>]],
      {i(1), i(0)}
    ),
    { condition = math }
  ),
  as({trig="__", name="subscript", wordTrig = false},
    fmta(
      [[_{<>}<>]],
      {i(1), i(0)}
    )
  ),
  as({trig="set", name="set brackets"},
    fmta(
      [[\{<>\}<>]],
      {i(1), i(0)}
    ),
    { condition = math }
  ),
  as({trig="ddel", name="del/del x"},
    fmta(
      [[\ddel{<>}<>]],
      {i(1), i(0)}
    ),
    { condition = math }
  ),
  as({trig="pdv", name="partial derivative"},
    fmta(
      [[\pdv{<>}{<>}<>]],
      {i(1), i(2), i(0)}
    ),
    { condition = math }
  )
}

-- local parse_snippet = ls.extend_decorator.apply(ls.parser.parse_snippet, decorator) --[[@as function]]
-- vim.list_extend(M, {
--   parse_snippet({ trig = "__", name = "subscript" }, "_{$1}$0"),
-- })

-- Use `<letter> to insert greek letters
local greek = {
  a = [[\alpha]],
  b = [[\beta]],
  g = [[\gamma]],
  w = [[\omega]],
  W = [[\Omega]],
  d = [[\delta]],
  D = [[\Delta]],
  e = [[\epsilon]],
  E = [[\\Epsilon]],
  h = [[\eta]],
  z = [[\zeta]],
  Z = [[\Zeta]],
  l = [[\lambda]],
  L = [[\Lambda]],
  m = [[\mu]],
  n = [[\nu]],
  s = [[\sigma]],
  S = [[\Sigma]],
  x = [[\xi]],
  X = [[\Xi]],
  f = [[\phi]],
  F = [[\Phi]],
  y = [[\psi]],
  Y = [[\Psi]],
  r = [[\rho]],
  q = [[\theta]],
  i = [[\iota]],
  I = [[\Iota]],
  p = [[\pi]],
  P = [[\Pi]],
}
local greek_snippets = {}
for latin, greek_command in pairs(greek) do
  table.insert(
    greek_snippets,
    as({trig=string.format("@%s", latin)},
      { t(greek_command) },
      { condition = math }
    )
  )
end
vim.list_extend(M, greek_snippets)


-- The following strings are automatically prepended with a backslash when in math mode
local auto_backslash_cmds = {
  "arcsin",
  "sin",
  "arccos",
  "cos",
  "arctan",
  "tan",
  "log",
  "ln",
  "exp",
  "ast",
  "star",
  "perp",
  "sup",
  "inf",
  "det",
  "min",
  "max",
  "argmax",
  "argmin",
  "del",
  "int"
}
local auto_backslash_snippets = {}
for _, str in ipairs(auto_backslash_cmds) do 
  table.insert(
    auto_backslash_snippets,
    as({trig=str, wordTrig = true},
      t(string.format("\\%s", str)),
      { condition = math }
    )
  )
end
vim.list_extend(M, auto_backslash_snippets)

-- lr()
local brackets = {
  p = {left = "(", right = ")"},
  b = {left = "\\[", right = "\\]"},
  c = {left = "\\{", right = "\\}"},
  a = {left = "<", right = ">"},
  norm = {left = [[\|]], right = [[\|]]}
}

local bracket_snippets = {}
for cmd, context in pairs(brackets) do
  trigger = string.format("lr%s", cmd)
  command = string.format("\\left%s <> \\right%s", context.left, context.right)
  table.insert(
    bracket_snippets,
    as({trig=trigger},
      {t([[\left]] .. context.left), i(1), t([[\right]] .. context.right), i(0)},
      { condition = math }
    )
  )
end
vim.list_extend(M, bracket_snippets)


local simple_abbr = {
  leq = {trigger = "<=", command = [[\le]]},
  geq = {trigger = ">=", command = [[\ge]]},
  implies = {trigger = "=>", command = [[\implies]]},
  impliedby = {trigger = "=<", command = [[\impliedby]]},
  exists = {trigger = "EE", command = [[\exists]]},
  forall = {trigger = "AA", command = [[\forall]]},
  to = {trigger = "->", command = [[\to]]},
  mapsto = {trigger = "!>", command = [[\mapsto]]},
  cdot = {trigger = "**", command = [[\cdot]]},
  setminus = {trigger = [[\\\]], command = [[\setminus ]]},
  mgreater = {trigger = ">>", command = [[\gg]]},
  mless = {trigger = "<<", command = [[\ll]]},
  sim = {trigger = "~~", command = [[\sim]]},
  subset = {trigger = "cc", command = [[\subset]]},
  circ = {trigger = "Oo", command = [[\circ]]},
  mid = {trigger = "||", command = [[\mid]]},
  notin = {trigger = "notin", command = [[\not\in]]},
  NatNum = {trigger = "NN", command = [[\N]]},
  Integers = {trigger = "ZZ", command = [[\Z]]},
  RatNum = {trigger = "QQ", command = [[\Q]]},
  RealNum = {trigger = "RR", command = [[\R]]},
  ComplexNum = {trigger = "CC", command = [[\C]]},
  Probability = {trigger = "PP", command = [[\P]]},
  cap = {trigger = "Nn", command = [[\cap]]},
  cup = {trigger = "UU", command = [[\cup]]},
  emptyset = {trigger = "OO", command = [[\0]]},
  times = {trigger = "xx", command = [[\times]]},
  nabla = {trigger = "nabl", command = [[\nabla]]},
  infinity = {trigger = "ooo", command = [[\infty]]},
  neq = {trigger = "!=", command = [[\neq]]},
  inn = {trigger = "inn", command = [[\in]]}
}


simple_abbr_snippets = {}
for name, args in pairs(simple_abbr) do
  table.insert(
    simple_abbr_snippets,
    as({trig = args.trigger, name=name},
      t(args.command),
      { condition = math}
    )
  )
end
vim.list_extend(M, simple_abbr_snippets)


local simple_envs = {
  pmatrix = {trigger = "pmat", env = "pmatrix"},
  cases = {trigger = "case", env = "cases"}
}
simple_envs_snippets = {}
for name, args in pairs(simple_envs) do
  table.insert(
    simple_envs_snippets,
    as({trig = args.trigger, name = name},
      {t({string.format("\\begin{%s}", args.env), ""}), 
        t"\t", i(0),
        t({"", string.format("\\end{%s}", args.env)})},
      { condition = math }
    )
  )
end
vim.list_extend(M, simple_envs_snippets)

-- Command snippets 

local commands = {
  mathcal = {trigger = "mcal", command = [[\mathcal]]},
  mathfrak = {trigger = "mfrak", command = [[\mathfrak]]},
  mathsc = {trigger = "msc", command = [[\mathsc]]},
  mathbf = {trigger = "mbf", command = [[\mathbf]]},
  mathbb = {trigger = "mbb", command = [[\mathbb]]},
  square_root = {trigger = "sq", command = [[\sqrt]]}
}
command_snippets = {}
for name, args in pairs(commands) do
  table.insert(
    command_snippets,
    as({trig = args.trigger, name=name},
      {t(args.command), t([[{]]), i(1), t([[}]]), i(0)},
      { condition = math}
    )
  )
end
vim.list_extend(M, command_snippets)

local simple_post_fix = {
  hat = {trigger = "hat", command = [[\hat]]},
  bar = {trigger = "bar", command = [[\overline]]},
  wig = {trigger = "wig", command = [[\widetilde]]},
  mathcal = {trigger = "cal", command = [[\mathcal]]},
  mathfrak = {trigger = "frak", command = [[\mathcal]]}
}
local simple_post_fix_snippets = {}
for name, args in pairs(simple_post_fix) do 
  table.insert(
    simple_post_fix_snippets,
    ps({trig = args.trigger, snippetType = "autosnippet"},
      {
        f(function(_, parent)
          return args.command .. [[{]] .. parent.snippet.env.POSTFIX_MATCH .. [[}]]
        end, {})
      },
      { condition = math}
    )
  )
end
vim.list_extend(M, simple_post_fix_snippets)

local sub_sup = {
  square = {trigger = "sr", command = [[^2]]},
  cube = {trigger = "cb", command = [[^3]]},
  inverse = {trigger = "inv", command = [[^{-1}]]},
  complement = {trigger = "compl", command = [[^c]]}
}
for name, args in pairs(sub_sup) do 
  table.insert(
    M,
    as({trig = args.trigger, snippetType = "autosnippet", wordTrig = false},
      {t(args.command)},
      { condition = math}
    )
  )
end



--[[
    Fraction snippets 
    -----------------
    Much of the following section is adapted from github.com/iurimateus/luasnip-latex-snippets.nvim
]]

local frac_no_parens = {
  f(function(_, snip)
    return string.format("\\frac{%s}", snip.captures[1])
  end, {}),
  t("{"),
  i(1),
  t("}"),
  i(0),
}

local frac = as({
  priority = 1000,
  trig = ".*%)/",
  wordTrig = true,
  regTrig = true,
  name = "() frac",
}, {
    f(function(_, snip)
      local match = snip.trigger
      local stripped = match:sub(1, #match - 1)

      i = #stripped
      local depth = 0
      while true do
        if stripped:sub(i, i) == ")" then
          depth = depth + 1
        end
        if stripped:sub(i, i) == "(" then
          depth = depth - 1
        end
        if depth == 0 then
          break
        end
        i = i - 1
      end

      local rv =
      string.format("%s\\frac{%s}", stripped:sub(1, i - 1), stripped:sub(i + 1, #stripped - 1))

      return rv
    end, {}),
    t("{"),
    i(1),
    t("}"),
    i(0),
  }, {
    condition = math
  })

local frac_snippets = {
  as({
    trig = "(\\?[%w]+\\?^%w)/",
    regTrig = true,
    name = "Fraction no ()",
  }, vim.deepcopy(frac_no_parens),
    { condition = math }
  ),

  as({
    trig = "(\\?[%w]+\\?_%w)/",
    regTrig = true,
    name = "Fraction no ()",
  }, vim.deepcopy(frac_no_parens),
    { condition = math }
  ),

  as({
    trig = "(\\?[%w]+\\?^{%w*})/",
    regTrig = true,
    name = "Fraction no ()",
  }, vim.deepcopy(frac_no_parens),
    { condition = math }
  ),

  as({
    trig = "(\\?[%w]+\\?_{%w*})/",
    regTrig = true,
    name = "Fraction no ()",
  }, vim.deepcopy(frac_no_parens),
    { condition = math}
  ),

  as({
    trig = "(\\?%w+)/",
    regTrig = true,
    name = "Fraction no ()",
  }, vim.deepcopy(frac_no_parens),
    { condition = math}
  ),
}

table.insert(frac_snippets, frac)
vim.list_extend(M, frac_snippets)

local auto_subscripts = {
  as({
    trig = "([%a]+)(%d)",
    regTrig = true,
    name = "auto subscript",
  }, {
      f(function(_, snip)
        return string.format("%s_%s", snip.captures[1], snip.captures[2])
      end, {}),
      i(0),
    }, {
      condition = math
    }),
  as({
    trig = "([%a]+)_(%d%d)",
    regTrig = true,
    name = "auto subscript 2",
  }, {
      f(function(_, snip)
        return string.format("%s_{%s}", snip.captures[1], snip.captures[2])
      end, {}),
      i(0),
    }, {
      condition = math
    })
}

vim.list_extend(M, auto_subscripts)


return M
