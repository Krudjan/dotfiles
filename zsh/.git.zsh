#
# Functions
#

# The name of the current branch
# Back-compatibility wrapper for when this function was defined here in
# the plugin, before being pulled in to core lib/git.zsh as git_current_branch()
# to fix the core -> git plugin dependency.
function current_branch() {
  git_current_branch
}

# Pretty log messages
function _git_log_prettily(){
  if ! [ -z $1 ]; then
    git log --pretty=$1
  fi
}
compdef _git _git_log_prettily=git-log

# Warn if the current branch is a WIP
function work_in_progress() {
  if $(git log -n 1 2>/dev/null | grep -q -c "\-\-wip\-\-"); then
    echo "WIP!!"
  fi
}

#
# Aliases
# (sorted alphabetically)
#

abbrev-alias g='git'

abbrev-alias ga='git add'
abbrev-alias gaa='git add --all'
abbrev-alias gapa='git add --patch'
abbrev-alias gau='git add --update'
abbrev-alias gav='git add --verbose'
abbrev-alias gap='git apply'

abbrev-alias gb='git branch'
abbrev-alias gba='git branch -a'
abbrev-alias gbd='git branch -d'
abbrev-alias gbda='git branch --no-color --merged | command grep -vE "^(\+|\*|\s*(master|develop|dev)\s*$)" | command xargs -n 1 git branch -d'
abbrev-alias gbD='git branch -D'
abbrev-alias gbl='git blame -b -w'
abbrev-alias gbnm='git branch --no-merged'
abbrev-alias gbr='git branch --remote'
abbrev-alias gbs='git bisect'
abbrev-alias gbsb='git bisect bad'
abbrev-alias gbsg='git bisect good'
abbrev-alias gbsr='git bisect reset'
abbrev-alias gbss='git bisect start'

abbrev-alias gc='git commit -v'
abbrev-alias gc!='git commit -v --amend'
abbrev-alias gcn!='git commit -v --no-edit --amend'
abbrev-alias gca='git commit -v -a'
abbrev-alias gca!='git commit -v -a --amend'
abbrev-alias gcan!='git commit -v -a --no-edit --amend'
abbrev-alias gcans!='git commit -v -a -s --no-edit --amend'
abbrev-alias gcam='git commit -a -m'
abbrev-alias gcsm='git commit -s -m'
abbrev-alias gcb='git checkout -b'
abbrev-alias gcf='git config --list'
abbrev-alias gcl='git clone --recurse-submodules'
abbrev-alias gclean='git clean -id'
abbrev-alias gpristine='git reset --hard && git clean -dfx'
abbrev-alias gcm='git checkout master'
abbrev-alias gcd='git checkout develop'
abbrev-alias gcmsg='git commit -m'
abbrev-alias gco='git checkout'
abbrev-alias gcount='git shortlog -sn'
abbrev-alias gcp='git cherry-pick'
abbrev-alias gcpa='git cherry-pick --abort'
abbrev-alias gcpc='git cherry-pick --continue'
abbrev-alias gcs='git commit -S'

abbrev-alias gd='git diff'
abbrev-alias gdca='git diff --cached'
abbrev-alias gdcw='git diff --cached --word-diff'
abbrev-alias gdct='git describe --tags $(git rev-list --tags --max-count=1)'
abbrev-alias gds='git diff --staged'
abbrev-alias gdt='git diff-tree --no-commit-id --name-only -r'
abbrev-alias gdw='git diff --word-diff'

function gdv() { git diff -w "$@" | view - }
compdef _git gdv=git-diff

abbrev-alias gf='git fetch'
abbrev-alias gfa='git fetch --all --prune'
abbrev-alias gfo='git fetch origin'

abbrev-alias gfg='git ls-files | grep'

abbrev-alias gg='git gui citool'
abbrev-alias gga='git gui citool --amend'

function ggf() {
  [[ "$#" != 1 ]] && local b="$(git_current_branch)"
  git push --force origin "${b:=$1}"
}
compdef _git ggf=git-checkout
function ggfl() {
  [[ "$#" != 1 ]] && local b="$(git_current_branch)"
  git push --force-with-lease origin "${b:=$1}"
}
compdef _git ggfl=git-checkout

function ggl() {
  if [[ "$#" != 0 ]] && [[ "$#" != 1 ]]; then
    git pull origin "${*}"
  else
    [[ "$#" == 0 ]] && local b="$(git_current_branch)"
    git pull origin "${b:=$1}"
  fi
}
compdef _git ggl=git-checkout

function ggp() {
  if [[ "$#" != 0 ]] && [[ "$#" != 1 ]]; then
    git push origin "${*}"
  else
    [[ "$#" == 0 ]] && local b="$(git_current_branch)"
    git push origin "${b:=$1}"
  fi
}
compdef _git ggp=git-checkout

function ggpnp() {
  if [[ "$#" == 0 ]]; then
    ggl && ggp
  else
    ggl "${*}" && ggp "${*}"
  fi
}
compdef _git ggpnp=git-checkout

function ggu() {
  [[ "$#" != 1 ]] && local b="$(git_current_branch)"
  git pull --rebase origin "${b:=$1}"
}
compdef _git ggu=git-checkout

abbrev-alias ggpur='ggu'
abbrev-alias ggpull='git pull origin "$(git_current_branch)"'
abbrev-alias ggpush='git push origin "$(git_current_branch)"'

abbrev-alias ggsup='git branch --set-upstream-to=origin/$(git_current_branch)'
abbrev-alias gpsup='git push --set-upstream origin $(git_current_branch)'

abbrev-alias ghh='git help'

abbrev-alias gignore='git update-index --assume-unchanged'
abbrev-alias gignored='git ls-files -v | grep "^[[:lower:]]"'
abbrev-alias git-svn-dcommit-push='git svn dcommit && git push github master:svntrunk'

abbrev-alias gk='\gitk --all --branches'
abbrev-alias gke='\gitk --all $(git log -g --pretty=%h)'

abbrev-alias gl='git pull'
abbrev-alias glg='git log --stat'
abbrev-alias glgp='git log --stat -p'
abbrev-alias glgg='git log --graph'
abbrev-alias glgga='git log --graph --decorate --all'
abbrev-alias glgm='git log --graph --max-count=10'
abbrev-alias glo='git log --oneline --decorate'
abbrev-alias glol="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'"
abbrev-alias glols="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --stat"
abbrev-alias glod="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'"
abbrev-alias glods="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --date=short"
abbrev-alias glola="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --all"
abbrev-alias glog='git log --oneline --decorate --graph'
abbrev-alias gloga='git log --oneline --decorate --graph --all'
abbrev-alias glp="_git_log_prettily"

abbrev-alias gm='git merge'
abbrev-alias gmom='git merge origin/master'
abbrev-alias gmt='git mergetool --no-prompt'
abbrev-alias gmtvim='git mergetool --no-prompt --tool=vimdiff'
abbrev-alias gmum='git merge upstream/master'
abbrev-alias gma='git merge --abort'

abbrev-alias gp='git push'
abbrev-alias gpd='git push --dry-run'
abbrev-alias gpf='git push --force-with-lease'
abbrev-alias gpf!='git push --force'
abbrev-alias gpoat='git push origin --all && git push origin --tags'
abbrev-alias gpu='git push upstream'
abbrev-alias gpv='git push -v'

abbrev-alias gr='git remote'
abbrev-alias gra='git remote add'
abbrev-alias grb='git rebase'
abbrev-alias grba='git rebase --abort'
abbrev-alias grbc='git rebase --continue'
abbrev-alias grbd='git rebase develop'
abbrev-alias grbi='git rebase -i'
abbrev-alias grbm='git rebase master'
abbrev-alias grbs='git rebase --skip'
abbrev-alias grev='git revert'
abbrev-alias grh='git reset'
abbrev-alias grhh='git reset --hard'
abbrev-alias groh='git reset origin/$(git_current_branch) --hard'
abbrev-alias grm='git rm'
abbrev-alias grmc='git rm --cached'
abbrev-alias grmv='git remote rename'
abbrev-alias grrm='git remote remove'
abbrev-alias grs='git restore'
abbrev-alias grset='git remote set-url'
abbrev-alias grss='git restore --source'
abbrev-alias grt='cd "$(git rev-parse --show-toplevel || echo .)"'
abbrev-alias gru='git reset --'
abbrev-alias grup='git remote update'
abbrev-alias grv='git remote -v'

abbrev-alias gsb='git status -sb'
abbrev-alias gsd='git svn dcommit'
abbrev-alias gsh='git show'
abbrev-alias gsi='git submodule init'
abbrev-alias gsps='git show --pretty=short --show-signature'
abbrev-alias gsr='git svn rebase'
abbrev-alias gss='git status -s'
abbrev-alias gst='git status'

# use the default stash push on git 2.13 and newer
autoload -Uz is-at-least
is-at-least 2.13 "$(git --version 2>/dev/null | awk '{print $3}')" \
  && abbrev-alias gsta='git stash push' \
  || abbrev-alias gsta='git stash save'

abbrev-alias gstaa='git stash apply'
abbrev-alias gstc='git stash clear'
abbrev-alias gstd='git stash drop'
abbrev-alias gstl='git stash list'
abbrev-alias gstp='git stash pop'
abbrev-alias gsts='git stash show --text'
abbrev-alias gstall='git stash --all'
abbrev-alias gsu='git submodule update'
abbrev-alias gsw='git switch'
abbrev-alias gswc='git switch -c'

abbrev-alias gts='git tag -s'
abbrev-alias gtv='git tag | sort -V'
abbrev-alias gtl='gtl(){ git tag --sort=-v:refname -n -l "${1}*" }; noglob gtl'

abbrev-alias gunignore='git update-index --no-assume-unchanged'
abbrev-alias gunwip='git log -n 1 | grep -q -c "\-\-wip\-\-" && git reset HEAD~1'
abbrev-alias gup='git pull --rebase'
abbrev-alias gupv='git pull --rebase -v'
abbrev-alias gupa='git pull --rebase --autostash'
abbrev-alias gupav='git pull --rebase --autostash -v'
abbrev-alias glum='git pull upstream master'

abbrev-alias gwch='git whatchanged -p --abbrev-commit --pretty=medium'
abbrev-alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "--wip-- [skip ci]"'
