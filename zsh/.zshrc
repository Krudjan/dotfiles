source ~/.antigen.zsh
#source ~/.completion.zsh

antigen use oh-my-zsh

#antigen bundle git
antigen bundle rsync
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle https://github.com/momo-lab/zsh-abbrev-alias


antigen theme https://github.com/dikiaap/dotfiles.git .oh-my-zsh/themes/oxide
antigen apply
export EDITOR='nvim'

abbrev-alias pas='sudo pacman -S'
abbrev-alias pac='sudo pacman'
abbrev-alias pacu='sudo pacman -Syu'
abbrev-alias eo="sudo eopkg"
alias off="sudo poweroff -f"
alias ping='ping -c5'
alias protokoll='cp ~/Dokumente/Latex/Vorlagen/Protokoll/Protokoll.tex Protokoll.tex'
alias brief='cp ~/Dokumente/Latex/Vorlagen/Brief/Brief.tex Brief.tex'
alias groß='cp ~/Dokumente/Latex/Vorlagen/Groß/Groß.tex Groß.tex'
alias flyer='cp ~/Dokumente/Latex/Vorlagen/Flyer/Flyer.tex Flyer.tex'
alias beamer='cp ~/Dokumente/Latex/Vorlagen/Beamer/BeamerMinimal.tex BeamerMinimal.tex'
alias vi='vim'
alias vim='nvim'
abbrev-alias ac='arduino-cli compile'
abbrev-alias au='arduino-cli upload -p $(ls /dev/ttyACM* | rofi -dmenu)'
alias wetter="curl 'wttr.in/Nordhausen'"
alias cat='bat'
alias ls='ls --color=auto'
alias yay='yay --nosudoloop'
abbrev-alias -g G='| rg'
abbrev-alias -g L='| less'
alias python='python3'
alias py='python3'
alias ipy='ipython'
alias ip='ip -c'
source ~/.git.zsh


# Expand aliases:
#function expand-alias() {
#	zle _expand_alias
#	zle self-insert
#}
#zle -N expand-alias
#bindkey -M main ' ' expand-alias

export XKB_DEFAULT_LAYOUT=de
export XKB_DEFAULT_VARIANT=neo

export PATH=$PATH:/home/gregor/.scripts:/home/gregor/go/bin/:/home/gregor/.local/bin/:home/gregor/.cargo/bin

if [ $TTY = '/dev/tty1' ]
then
	sway
fi

bindkey '\e[H' beginning-of-line
bindkey '\e[F' end-of-line

export NNN_TMPFILE="/tmp/nnn"
n() {
    nnn "$@"

    if [ -f $NNN_TMPFILE ]
    then
        . $NNN_TMPFILE
        rm $NNN_TMPFILE
    fi
}

mkcd() {
    mkdir -p "$1" &&
    cd "$1"
}

function makeweekdirs() {
    # In each subdirectory make a week directory for each week given by the first argument.
    for dir in */
    do
        for i in {1..$1}
        do
            mkdir -p ${dir}Woche$(printf "%02d" $i);
        done
    done
}

function cdu() {
    doc_dir="$HOME/Nextcloud/Documents/Uni"

    year=$(date +%Y)
    month=$(date +%m)

    if [[ $month -le 9 && $month -ge 4 ]]
    then
        cd "$doc_dir/$year-SoSe"
    elif [[ $month -ge 10 ]]
    then
        cd "$doc_dir/$year-WiSe"
    elif [[ $month -le 3 ]]
    then
        old_year=$(echo "$year - 1" | bc)
        cd "$doc_dir/$old_year-WiSe"
    fi

    cd $({fd --type d && echo .} | fzf)
}

function twitch () {
    list_of_streamers="vlesk\nriolutm\ntrilluxe\npietsmiet\ndhalucard\ncustom"
    result=$(echo $list_of_streamers | fzf)
    if [[ $result == "custom" ]]
    then
        echo -n "Please enter Name: "
        read streamer
        streamlink "twitch.tv/$streamer" best > /dev/null &
    else
        streamlink "twitch.tv/$result" best > /dev/null &
    fi
    if [[ $? != 0 ]]
    then
        echo "Fehler"
    fi
    disown
}

#if [[ -n $NEW_TERM ]]
#then
#    ~/.scripts/tmux.sh
#fi

